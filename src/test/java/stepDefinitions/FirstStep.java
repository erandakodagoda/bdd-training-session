package stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

import java.util.List;

@RunWith(Cucumber.class)
public class FirstStep{

    @Given("^I landed on home screen$")
    public void ILandedonHomePage(){
        System.out.println("I Landed on Home");
    }

    @When("^I tap menu icon$")
    public void ITapOnMenuIcons(){
        System.out.println("I Tap on Menu Icons");
    }
    @Then("^I should see left menu$")
    public void IShouldSeeLeftMenu(){
        System.out.println("I See Left Menu");
    }
    @When("^I enter username to username field (.*)$")
    public void enterUsername(String username){
        System.out.println("Username Entered :"+username);
    }
    @When("^I enter password to password field (.*)$")
    public void enterPassword(String password){
        System.out.println("Entered Password :"+password);
    }
    @When("^I click on login button$")
    public void iclickOnLoginBtn(){
        System.out.println("Login Btn Clicked");
    }
    @Then("^I see Home Page$")
    public void seeHomePage(){
        System.out.println("Home Page");
    }
    @Then("^I see Logout button (enabled|disabled)$")
    public void iseeLogoutButton(String flag){
        System.out.println("I see Logout Button enabled: "+flag);
    }
    @Given("^I send list of users:$")
    public void listUsers(List<String> users){
        for (String user : users) {
            System.out.println("Username is :"+ user);
        }
    }

}
