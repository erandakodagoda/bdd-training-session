package stepDefinitions;


import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    @Before
    public void runBefore(){
        System.out.println("Run Before");
    }
    @After
    public void runAfter(){
        System.out.println("Run After");
    }
}
