Feature: Test Feature

  Background:
    Given I landed on home screen


    @smoke
    @regression
  Scenario: When I tap on menu I should see left menu
    When I tap menu icon
    Then I should see left menu

      @regression
  Scenario: When I tap on menu I should see left menu 2
    When I tap menu icon
    Then I should see left menu

  Scenario Outline: As a user I should be able to Pass Username and Password and Login
  When I enter username to username field <username>
  And I enter password to password field <password>
  And I click on login button
  Then I see Home Page
  And I see Logout button disabled

  Examples:
  |username|password|
  |test    |pass    |
  |test2   |pass2   |

  Scenario: I pass users list into steps
    Given I send list of users:
    |Mark|
    |James|
    |Jhon |

